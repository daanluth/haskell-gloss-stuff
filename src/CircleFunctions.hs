import Graphics.Gloss
import Data.Fixed (mod')

-- all hail the tau gods
tau :: Float
tau = 2 * pi


width :: Float
height :: Float
width = 500
height = 500

main :: IO ()
main = animate (InWindow "Circle Functions" (floor width, floor height) (0, 0)) black
  (Scale width height . picture)

sign :: Float -> Float
sign x
  | x < 0 = -1
  | x >= 0 = 1

picture :: Float -> Picture
picture t =
  pictures [
  Color white $ Circle r,
  Color (makeColor 1 0 1 1) $ line [(0,0),(dx, dy)],

  -- cosine
  Color white $ line [(dx,0),(sign dx * r, 0)],
  Color white $ line [(0,0),(sign dx * (- r), 0)],
  Color red $ line [(0,0),(dx,0)],
  Color white $ line [(dx,0),(dx, dy)],

  -- sine
  Color white $ line [(0,dy),(0, sign dy * r)],
  Color white $ line [(0,0),(0, sign dy * (- r))],
  Color blue $ line [(0,0),(0,dy)],
  Color white $ line [(0,dy),(dx,dy)],

  -- tangent
  Color yellow $ line [((sign dx * r),0),(secdx, 0)],
  Color orange $ line [(0,(sign dy * r)),(0,cscdy)],
  Color green $ line [(0,cscdy),(secdx, 0)],
  Color white $ line [(secdx,0),(sign dx,0)],
  Color white $ line [(sign (- dx) * r,0),(sign (- dx),0)],
  Color white $ line [(0,cscdy),(0,sign dy)],
  Color white $ line [(0,sign (- dy) * r),(0,sign (- dy))],

  -- right angles
  Color white $ line [(dx,(sign dy * (r/25))),
                      (dx - (sign dx * (r/25)),(sign dy * (r /25))),
                      (dx - (sign dx * (r/25)), 0)],
  Color white $ line [((sign dx * (r/25)), dy),
                      ((sign dx * (r /25)), dy - (sign dy * (r/25))),
                      (0, dy - (sign dy * (r/25)))],
  Color white $ line [((24/25) * r * cos theta, (24/25) * r * sin theta),
                      ((24/25) * r * cos (theta - (1/25)), (24/25) * r * sin (theta - (1/25))),
                      (r * cos (theta - (1/25)), r * sin (theta - (1/25)))],

  -- angle
  Color green $ arc 0 ((theta / tau) * 360) (r / 10)
  ]
  where theta = (mod' (t+0.0000001) 30 / 30) * tau
        r = 0.3
        dx = cos theta * r
        dy = sin theta * r
        secdx = (1 / (cos theta)) * r
        cscdy = (1 / (sin theta)) * r
