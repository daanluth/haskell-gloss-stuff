import Graphics.Gloss

width :: Int
height :: Int
width = 500
height = 500

main :: IO ()
main = animate (InWindow "Sine Wave" (width, height) (2, 2)) black picture

picture :: Float -> Picture
picture t =
  Translate (- (fromIntegral $ div width 2)) 0 $
  Color white $
  pictures [line $ map (\x -> (x, animatedSine x t)) [0.0..(fromIntegral width)],
            Translate circleX (animatedSine circleX t) $ Circle 5.0,
            line [(circleX, -75.0), (circleX, 75.0)]]
  where circleX = fromIntegral (div width 2)

animatedSine :: Float -> Float -> Float
animatedSine x time =
  sin ((x * 2 * pi * waves / fromIntegral height) - (time * waves)) *
  sin (x * pi / (fromIntegral width)) * 50.0
  where waves = 5
