import Graphics.Gloss

main :: IO ()
main = display (InWindow "Recursive Tree" (500, 500) (2, 2)) black picture

picture =
  Color white $
  translate 0 (- (dy * (sqrt 2))) $
  tree 10 dy
  where dy = 200

thickLine :: Point -> Point -> Float -> Picture
thickLine (x1, y1) (x2, y2) width = Polygon [(x1-hw, y1), (x2-hw,y2),(x2+hw,y2),(x1+hw, y1)]
  where hw = width / (2 :: Float)

tree :: Int -> Float -> Picture
tree 0 h = line [(0, 0), (0, h)]
tree n h =
  pictures [curr,
            translate 0 h $ rotate (- 45) branch,
            translate 0 h $ rotate 45 branch]
  where
    curr = line [(0, 0), (0, h)]
    branch = tree (n - 1) (h * 0.59)
