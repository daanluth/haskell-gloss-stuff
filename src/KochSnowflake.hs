import Graphics.Gloss

width :: Float
height :: Float
width = 500
height = 500

main :: IO ()
main = animate (InWindow "Koch Snowflake Animation" (floor width, floor height) (20, 20)) black
  (\time -> let n = 6 - abs (mod (floor time) 12 - 6) in
       Color white $ Translate (- (width / 2)) 0 $ Scale width height $ picture n)

picture :: Integer -> Picture
picture n =
  Translate ((- 1) / 4) 0 $
  Scale 1.5 1.5 $
  pictures [
  Translate (1 / 3) (1 / 6) side,
  Translate (1 / 3) (1 / 6) $ Scale 1 (- 1) $ Rotate (- 60) side,
  Translate (2 / 3) (1 / 6) $ Scale (- 1) 1 $ Rotate 60 $ Scale 1 (- 1) side
  ]
  where side = Scale (1/3) (1/3) $ surface n

surface :: Integer -> Picture
surface 0 = line [(0, 0), (1, 0)]
surface n = pictures [
  next,
  Translate (1 / 3) 0 $ Rotate (- 60) next,
  Translate (1 / 2) (1 / 3.5) $ Rotate 60 next,
  Translate (2 / 3) 0 next
  ]
  where next = Scale (1 / 3) (1 / 3) $ surface (n - 1)
