# Gloss Stuff 
### Some Stuff i made with [Haskell](https://www.haskell.org/)/[Gloss](https://hackage.haskell.org/package/gloss)

### Fractal Tree
A recursively defined fractal tree, how original. Not even animated.  
  
![Fractal Tree Image](images/FractalTree.png)

#### Sine Wave
Mostly just looks cool, illustrates that a wave is just a bunch of points moving up and down  
  
![Sine Wave Image](images/SineWaveDemo.png)

#### Koch Snowflake
A animation that constructs the [Koch Snowflake](https://en.wikipedia.org/wiki/Koch_snowflake) one step at the time,  
the animation stops after 6 iterations, and returns back to the first iteration.  
  
![Koch Snowflake Image](images/KochSnowflakeDemo.png)

#### Circle Functions
A animation illustrating the relation 
between [sine, cosine and tangent](https://en.wikipedia.org/wiki/Trigonometric_functions) as defined  
using a unit circle  
  
![Circle Function Image](images/CircleFunctionsDemo.png)

## Usage:
the binaries are for linux, you might need to install the `freeglut3` package (`# apt install freeglut3`)  
if you don't already have it installed by default  
the scripts were made using [The Glasgow haskell compiler](https://www.haskell.org/ghc/).  
You do need to install [Gloss](https://hackage.haskell.org/package/gloss) to use/compile them.  
